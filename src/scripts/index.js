$(document).ready(function() {
  var isMobile = false;

  if($('#mobile-check').css('display') == 'none') {
    isMobile = true;
  }

  $('.menu__icon__wrapper').on('click', function() {
    $('.menu__icon').toggleClass('open');
    $('.menu__list').toggleClass('open');
  });

  $('.menu--black .menu__icon__wrapper').on('click', function() {
    $('.menu__overlay').toggleClass('active');
  });

  $('.menu__overlay').on('click', function() {
    $('.menu__icon').toggleClass('open');
    $('.menu__list').toggleClass('open');
    $('.menu__overlay').toggleClass('active');
  });

  $('.press').masonry({
  	itemSelector: '.press__box',
  	columnWidth: '.press__box',
  	percentPosition: true
  })

  if ($.cookie('hasShownModal') !== 'true') {
    $.cookie('hasShownModal', 'true');
    $('.modal').addClass('active');
    $('.modal__close').on('click', function() {
      $('.modal').removeClass('active');
    });
  }

  $(document).on("scroll", function () {
    var header = $('.header--default');
    var logo = $('.logo.white');
    var language = $('.header__language');
    var icons = $('.header__social__link .white');
    var menu = $('nav.menu');
    var scrollPosition = $(document).scrollTop();

    if (!header.length) return;

    function addBlackClasses () {
      header.addClass('header--black');
      logo.addClass('black');
      language.addClass('header__language--black');
      icons.each(function () {$(this).addClass('black')});
      menu.addClass('menu--black');
      if (!$('.menu__overlay').length) {
        menu.append($('<div class="menu__overlay"></div>'));

        $('.menu--black .menu__icon__wrapper').on('click', function() {
          $('.menu__overlay').toggleClass('active');
        });

        $('.menu__overlay').on('click', function() {
          $('.menu__icon').toggleClass('open');
          $('.menu__list').toggleClass('open');
          $('.menu__overlay').toggleClass('active');
        });
      }
    }

    function removeBlackClasses () {
      header.removeClass('header--black');
      logo.removeClass('black');
      language.removeClass('header__language--black');
      menu.removeClass('menu--black');
      icons.each(function () {$(this).removeClass('black')});

      if ($('.menu__overlay').length) {
        $('.menu__overlay').remove();
      }
    }

    if (!isMobile && scrollPosition > 745) {
      addBlackClasses();
    } else if (isMobile && scrollPosition > 445) {
      addBlackClasses();
    } else {
      removeBlackClasses();
    }
  });

  lightbox.option({
    'showImageNumberLabel': false,
    'disableScrolling': true
  })
});
